from enum import Enum

import colorama
from colorama import Fore, Back, Style

from enum import IntEnum
class Color(Enum):
    CADET_BLUE = 1
    CHARTREUSE = 2
    ORANGE = 3
    RED = 4
    YELLOW = 5

class Letter(IntEnum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6
    G = 7
    H = 8

class Position(object):
    def __init__(self, column: Letter, row: int):
        self.column = column
        self.row = row

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __str__(self):
        return f"{self.column.name}{self.row}"

    __repr__ = __str__

class Ship(object):
    def __init__(self, name: str, size: int, color: Color):
        self.name = name
        self.size = size
        self.color = color
        self.positions = []

    def add_position(self, input: str):
        letter = Letter[input.upper()[:1]]
        number = int(input[1:])
        position = Position(letter, number)


        self.positions.append(Position(letter, number))

    def __str__(self):
        return f"{self.color.name} {self.name} ({self.size}): {self.positions}"

    __repr__ = __str__


class Cell():
    def __init__(self, value):
        self.value = value
        self.color = Fore.BLUE


class Board():
    def __init__(self):
        self.board = []
        for i in range (0,8):
            row = []
            for j in range (0,8):
                row.append(Cell("O"))
            self.board.append(row)


    def __str__(self):
        print("    A   B   C   D   E   F   G   H")
        print("    -----------------------------")
        i=0
        for row in self.board:
            i +=1
            print(i , "| ", end='')
            for cell in row:
                print(cell.color + cell.value, "  " +  Style.RESET_ALL, end='')
                #print(" ")
                #print(self.board[1][1])
            print("\n")

        return ""

    def setHit(self, position):
        cell = self.board[position.row-1][position.column.value-1]
        cell.value = 'X'
        cell.color = Fore.RED

    def setMiss(self, position):
        cell = self.board[position.row-1][position.column.value-1]
        cell.value = 'O'
        cell.color = Fore.YELLOW

    __repr__ = __str__


if __name__ == '__main__':
    ploca = Board()

    ploca.setHit(Position(column=Letter.A, row=3))
    print(ploca)