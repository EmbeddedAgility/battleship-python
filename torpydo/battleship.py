import random

import colorama
from colorama import Fore, Back, Style

from torpydo.ship import Color, Letter, Position, Ship, Board
from torpydo.game_controller import GameController

myFleet = []
enemyFleet = []


board = Board()

def main():
    colorama.init()
    print(Fore.YELLOW + r"""
                                    |__
                                    |\/
                                    ---
                                    / | [
                             !      | |||
                           _/|     _/|-++'
                       +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                        Welcome to Battleship                         BB-61/
 \_________________________________________________________________________|""" + Style.RESET_ALL)

    print()
    print(board)

    initialize_game()

    start_game()

def start_game():
    global myFleet, enemyFleet

    print(r'''
                  __
                 /  \
           .-.  |    |
   *    _.-'  \  \__/
    \.-'       \
   /          _/
   |      _  /
   |     /_\
    \    \_/
     """"""""''')

    print(board)

    while True:
        print()
        print("Player, it's your turn")
        position = parse_position(input("Enter coordinates for your shot :"))
        if position.row > 8 or int (position.column) > 8 or position.row <= 0 or int(position.column) <= 0:
            print('MISS - out of playing field')
            continue
        
        is_hit = GameController.check_is_hit(enemyFleet, position)
        if is_hit:
            print(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

            board.setHit(position)
        else:
            board.setMiss(position)

        print(board)

        print("Yeah ! Nice hit !" if is_hit else "Miss")

        position = get_random_position()
        is_hit = GameController.check_is_hit(myFleet, position)
        print()
        print(f"Computer shoot in {position.column.name}{position.row} and {'hit your ship!' if is_hit else 'miss'}")
        if is_hit:
            print(r'''
                \          .  ./
              \   .:"";'.:..""   /
                 (M^^.^~~:.'"").
            -   (/  .    . . \ \)  -
               ((| :. ~ ^  :. .|))
            -   (\- |  \ /  |  /)  -
                 -\  \     /  /-
                   \  \   /  /''')

def parse_position(input: str):
    letter = Letter[input.upper()[:1]]
    number = int(input[1:])
    position = Position(letter, number)

    return Position(letter, number)

def get_random_position():
    rows = 8
    lines = 8

    letter = Letter(random.randint(1, lines))
    number = random.randint(1, rows)
    position = Position(letter, number)

    return position

def initialize_game():
    initialize_myFleet()

    initialize_enemyFleet()

def initialize_myFleet():
    global myFleet

    myFleet = GameController.initialize_ships()

    print("Please position your fleet (Game board has size from A to H and 1 to 8) :")

    for ship in myFleet:
        print()
        print(f"Please enter the positions for the {ship.name} (size: {ship.size})")
        availablePoints = []
        firstCol = {}
        firstRow = {}
        for i in range(ship.size):
            position_input = input(f"Enter position {i} of {ship.size} (i.e A3):")
            letter = Letter[position_input.upper()[:1]]
            number = int(position_input[1:])
            if int(letter) < int(letter.A) or int(letter) > int(letter.H):
                print(f"Invalid input")
                exit(0)
            if number < 0 or number > 8:
                print(f"Invalid input")
                exit(0)
            if checkIfShipsExistsOnPoint(letter,number, myFleet):
                print(f"SHIP EXISTS ON POINT")
                exit(0)
            if i == 0:
                availablePoints = getAvailablePoints(ship, myFleet, letter, number)
            if not inAvailablePoints(letter, number, availablePoints):
                print(f"INVALID POINT")
                exit(0)
            if i != 0 and changedOrientation(firstCol, firstRow, letter, number):
                print(f"ORIENTATION CHANGED POINT")
                exit(0)

            firstCol = letter
            firstRow = number
            ship.add_position(position_input)
def changedOrientation(firstCol, firstRow, letter, number):
    if firstCol != letter and firstRow != number:
        return True
    return False
def inAvailablePoints(letter, number, points):
    for point in points:
        l = int(point.column)
        n = point.row
        if int(letter) == l and n == number:
            return True

    return False

def getAvailablePoints(ship, fleet, letter,number):
    points = []
    tempPoints = []
    
    # CheckUpper
    lastUpper = int(letter) - ship.size
    firstUpper = int(letter)
    status = True
    if lastUpper > 0:
        while firstUpper > lastUpper:
            if checkIfShipsExistsOnPoint(firstUpper, number, fleet):
                status = False
                break
            else:
                tempPoints.append(Position(Letter(firstUpper),number))
                firstUpper = firstUpper - 1

    if status == True:
        points = points + tempPoints        

     # CheckLower
    lastLower = int(letter) + ship.size
    firstLower = int(letter)
    status = True
    tempPoints = []
    if lastLower < 9:
        while firstLower < lastLower:
            if checkIfShipsExistsOnPoint(firstLower, number, fleet):
                status = False
                break
            else:
                tempPoints.append(Position(Letter(firstLower),number))
                firstLower = firstLower + 1

    if status == True:
        points = points + tempPoints

    # CheckRight
    lastRight =  number + ship.size
    firstRight = number
    status = True
    tempPoints = []
    if lastRight < 9:
        while firstRight < lastRight:
            if checkIfShipsExistsOnPoint(letter, firstRight, fleet):
                status = False
                break
            else:
                tempPoints.append(Position(Letter(letter),firstRight))
                firstRight = firstRight + 1

    if status == True:
        points = points + tempPoints
        
     # CheckLeft
    lastLeft =  number - ship.size
    firstLeft = number
    status = True
    tempPoints = []
    if lastLeft > 0:
        while firstLeft > lastLeft:
            if checkIfShipsExistsOnPoint(letter, firstLeft, fleet):
                status = False
                break
            else:
                tempPoints.append(Position(Letter(letter),firstLeft))
                firstLeft = firstLeft - 1

    if status == True:
        points = points + tempPoints


    status = True
    print(points)
    return points

def checkIfShipsExistsOnPoint(letter, number, myFleet):
    for ship in myFleet:
        for i in ship.positions:
            column = i.column
            row = i.row
            if int(letter) == int(column) and row == number:
                return True
    return False

def initialize_enemyFleet():
    global enemyFleet

    enemyFleet = GameController.initialize_ships()

    enemyFleet[0].positions.append(Position(Letter.B, 4))
    enemyFleet[0].positions.append(Position(Letter.B, 5))
    enemyFleet[0].positions.append(Position(Letter.B, 6))
    enemyFleet[0].positions.append(Position(Letter.B, 7))
    enemyFleet[0].positions.append(Position(Letter.B, 8))

    enemyFleet[1].positions.append(Position(Letter.E, 6))
    enemyFleet[1].positions.append(Position(Letter.E, 7))
    enemyFleet[1].positions.append(Position(Letter.E, 8))
    enemyFleet[1].positions.append(Position(Letter.E, 9))

    enemyFleet[2].positions.append(Position(Letter.A, 3))
    enemyFleet[2].positions.append(Position(Letter.B, 3))
    enemyFleet[2].positions.append(Position(Letter.C, 3))

    enemyFleet[3].positions.append(Position(Letter.F, 8))
    enemyFleet[3].positions.append(Position(Letter.G, 8))
    enemyFleet[3].positions.append(Position(Letter.H, 8))

    enemyFleet[4].positions.append(Position(Letter.C, 5))
    enemyFleet[4].positions.append(Position(Letter.C, 6))

if __name__ == '__main__':
    main()
